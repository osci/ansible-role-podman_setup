# Ansible role to setup podman for rootless containers

## Introduction

This role installs podman and needed plugins and tools to run podman
rootless workloads. It also creates the user needed to run these
workloads and setup their subuids/subgids as well as enabling long user
sessions (linguering).
